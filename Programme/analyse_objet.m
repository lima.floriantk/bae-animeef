function ret = analyse_objet(data,data1)
x1 = size(data,1);
y1 = size(data,2);
x2 = size(data1,1);
y2 = size(data1,2);

if x1 > x2
    x = x2;
else
    x = x1;
end

if y1 > y2
    y = y2;
else
    y = y1;
end


srfc = analyse_surface(data,1,x1,y1);

prmtr = analyse_perimetre(data,1,x1,y1);

idc_crcl = indice_circula(prmtr,srfc,1);

pstn = analyse_position(data,1,x1,y1);

cgm = ctr_grv(data,1);

frt = analyse_feret(data, 1);

smm = ITot(data1,data,1,x,y);

moy = IMoy(data1,data,1,x,y);

pic = Centre_pics(data1,data,1,x,y);

ecrt_tp = Sig(data1,data,1,x,y);

ret = struct('srfc',srfc,'prmtr',prmtr,'idc_crcl',idc_crcl,'pstn',pstn,'cgm',cgm,'frt',frt,'smm',smm,'moy',moy,'pic',pic,'ecrt_tp',ecrt_tp);
end



function ret = analyse_surface(data,nbr_couche,x,y)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if (data(j,k,i) == 1)
                ret(i) = ret(i) + 1;
            end
        end
    end
end
end



function ret = analyse_perimetre(data_masq,nbr_couche,x,y)
BW = zeros(x,y,nbr_couche);

for i = 1:nbr_couche
    BW(:,:,i) = bwperim(data_masq(:,:,i));
end

ret = analyse_surface(BW,nbr_couche,x,y);
end



function ret = indice_circula(perimetre,air,index)
ret = zeros(index,1);

for i = 1:index
    ret(i) = (perimetre(i) * perimetre(i)) / (4 * pi *air(i));
end
end



function ret = analyse_position(data,nbr_couche,x,y)
ret = zeros(nbr_couche,4);

for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if (data(j,k,i) == 1 && (ret(i,1) == 0 || ret(i,1) > k))
                ret(i,1) = k;
            end
            if (data(j,k,i) == 1 && (ret(i,2) == 0 || ret(i,2) > j))
                ret(i,2) = j;
            end
            if (data(j,k,i) == 1 && (ret(i,3) == 0 || ret(i,3) < k))
                ret(i,3) = k;
            end
            if (data(j,k,i) == 1 && (ret(i,4) == 0 || ret(i,4) < j))
                ret(i,4) = j;
            end
        end
    end
end
end



function ret = ctr_grv(data,nbr_couche)
%obj_max = recup_nbr_obj_max(x.nbr_obj,x.nbr_couche);
%disp (obj_max)
ret = zeros(nbr_couche,2);
for i = 1:nbr_couche
    %disp(i)
    stats = regionprops(data(:,:,i));
    %disp(i)
    %disp(data(:,:,i))
    %disp(stats)
    ret(i,1) = round(stats.Centroid(1));
    ret(i,2) = round(stats.Centroid(2));
    %contourf(nuage_cg)
end
end



function ret = analyse_feret(data_masq,nbr_masq)
ret = zeros(nbr_masq,3);
for i = 1:nbr_masq
    min = bwferet(data_masq(:,:,i),'MinFeretProperties');

    %disp(min)
    ret(i,1) = min.MinDiameter(1);
    %disp(ret)

    max = bwferet(data_masq(:,:,i),'MaxFeretProperties');

    ret(i,2) = max.MaxDiameter(1);

    ret(i,3) = min.MinAngle(1) - max.MaxAngle(1);

    if (ret(i,3) > 180)
        ret(i,3) = ret(i,3) - 360;
    end
    if (ret(i,3) < (-180))
        ret(i,3) = ret(i,3) + 360;
    end
    if (ret(i,3) < 0)
        ret(i,3) = ret(i,3) * -1;
    end
    %disp(ret(i,3))

end
end

function ret = ITot(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if (data_masq(j,k,i) == 1)
                ret(i) = ret(i) + data(j,k);
            end
        end
    end
end
end

function ret = IMoy(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    cpt = 0;
    for j = 1:x
        for k = 1:y
            if (data_masq(j,k,i) == 1)
                ret(i) = ret(i) + data(j,k);
                cpt = cpt + 1;
            end
        end
    end
    ret(i) = ret(i)/cpt ;
end
end

function ret = Centre_pics(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,3);
for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if ( data_masq(j,k,i) == 1)
                if ret(i,1) == 0 && ret(i,2) == 0
                    ret(i,1) = j;
                    ret(i,2) = k;
                    ret(i,3) = data(j,k);
                end
                if data(j,k) > data(ret(i,1),ret(i,2))
                    ret(i,1) = j;
                    ret(i,2) = k;
                    ret(i,3) = data(j,k);
                end
            end
        end
    end
end
end

function ret = Sig(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,1);
moyenne = IMoy(data,data_masq,nbr_couche,x,y);

for i = 1:nbr_couche
    cpt = 0;
    for j = 1:x
        for k = 1:y
            if (data_masq(j,k,i) == 1)
                tmp = data(j,k) - moyenne(i);
                if (tmp < 0)
                    tmp = tmp * (-1);
                end
                ret(i) = ret(i) + tmp * tmp ;
                cpt = cpt + 1;
            end
        end
    end
    ret(i) = ret(i)/cpt ;
    ret(i) = sqrt(ret(i));
end
end