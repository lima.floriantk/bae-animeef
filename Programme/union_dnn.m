function ret = union_dnn(data1,data2)
x1 = size(data1,1);
x2 = size(data2,1);
y1 = size(data1,2);
y2 = size(data2,2);


if (x1 > x2)
    xm = x1;
else
    xm = x2;
end
if (y1 > y2)
    ym = y1;
else
    ym = y2;
end

ret = zeros(xm,ym);

for i = 1:x1
    for j = 1:y1
        if (data1(i,j) == 1)
            ret(i,j) = 1;
        end
    end
end

for i = 1:x2
    for j = 1:y2
        if (data2(i,j) == 1)
            ret(i,j) = 1;
        end
    end
end
end