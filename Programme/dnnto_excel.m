function dnnto_excel(file_name,data,rep_echtll)
%name_file = strrep(file_name,'.mat','.xlsx');
%copyfile("Feuille_vierge.xlsx",name_file);
if (exist("Feuille_vierge.xlsx","file") == 0)
    xlswrite("Feuille_vierge.xlsx",[cellstr(""),cellstr("")],'Données');
    xlswrite("Feuille_vierge.xlsx",[cellstr(""),cellstr("")],'Information');
end
[~, ~, raw] = xlsread('Feuille_vierge.xlsx','Données');
[~, ~, raw2] = xlsread('Feuille_vierge.xlsx','Information');
s = load(file_name);
%raw() = s.data;
%raw() = s.data_masq;

raw(1,3) = cellstr("Paramètre Géométrique");
raw(1,17) = cellstr("Paramètre de fluorescence");
raw(2,4) = cellstr("Position");
raw(2,8) = cellstr("Centre grav");
raw(2,13) = cellstr("Diamètre de ferret");
raw(2,17) = cellstr("Pics");
raw(2,19) = cellstr("Quantitatif");
raw(3,1) = cellstr("Masque");
raw(3,2) = cellstr("Part");
raw(3,3) = cellstr("Nbr objt");
raw(3,4) = cellstr("Em min(nm)");
raw(3,5) = cellstr("Ex min(nm)");
raw(3,6) = cellstr("Em max(nm)");
raw(3,7) = cellstr("Ex max(nm)");
raw(3,8) = cellstr("x");
raw(3,9) = cellstr("y");
raw(3,10) = cellstr("Perimetre (pixel)");
raw(3,11) = cellstr("Surface (pixel)");
raw(3,12) = cellstr("Idc circularité");
raw(3,13) = cellstr("Min");
raw(3,14) = cellstr("Max");
raw(3,15) = cellstr("Facteur d'elongation");
raw(3,16) = cellstr("angle °");
raw(3,21) = cellstr("x = Em(nm)");
raw(3,22) = cellstr("y = Ex(nm)");
raw(3,17) = cellstr("Somme IFTot");
raw(3,18) = cellstr("IF Moy");
raw(3,19) = cellstr("Sig");
raw(3,20) = cellstr("IF Max");

raw2(5,1) = cellstr("Domaine optique");
raw2(9,1) = cellstr("Pas d'acquisition");
raw2(2,2) = cellstr("Nom");
raw2(3,2) = cellstr("Solvant");
raw2(4,2) = cellstr("Provenance");
raw2(5,2) = cellstr("Excitation min");
raw2(6,2) = cellstr("Excitation max");
raw2(7,2) = cellstr("Emission min");
raw2(8,2) = cellstr("Emission max");
raw2(9,2) = cellstr("Excitation");
raw2(10,2) = cellstr("Emission");
raw2(11,2) = cellstr("PM");
raw2(12,2) = cellstr("Bande passante");
raw2(13,2) = cellstr("Vitesse d'acquisition");
raw2(14,2) = cellstr("Réponse");
raw2(15,2) = cellstr("Appareillage");


for i = 1:(s.nbr_couche)
    %Numero du masque
    raw(3+i,1) = num2cell(i);
    %Pourcentage du masque
    raw(3+i,2) = num2cell(strcat(int2str((s.nbr_couche + 1 - i)/s.nbr_couche * 100),"%"));
    %Surface du masque
    raw(3+i,11) = num2cell(s.srfc(i));
    %Position/encadrement du masque
    %%

    min_lo1 = cell2mat(data(4));
    min_lo2 = cell2mat(data(6));
    pas1 = cell2mat(data(8));
    %disp(pas1)
    pas2 = cell2mat(data(9));
    raw(3+i,4) = num2cell((s.pstn(i,1)-1)*pas2 + min_lo2);
    disp(s.pstn(i,2))
    raw(3+i,5) = num2cell((s.pstn(i,2)-1)*pas1 + min_lo1);

    raw(3+i,6) = num2cell((s.pstn(i,3)-1)*pas2 + min_lo2);

    raw(3+i,7) = num2cell((s.pstn(i,4)-1)*pas1 + min_lo1);
    %%
    %Somme intensité F
    raw(3+i,17) = num2cell(s.smm(i));
    %Perimetre
    raw(3+i,10) = num2cell(s.prmtr(i));
    %Nombre d'objet
    raw(3+i,3) = num2cell(s.nbr_obj(i));
    %IF moy
    raw(3+i,18) = num2cell(s.moy(i));
    %Indice de circularité
    raw(3+i,12) = num2cell(s.idc_crcl(i));
    %Ecart type
    raw(3+i,19) = num2cell(s.ecrt_tp(i));
    %Centre de pics
    %%

    raw(3+i,22) = num2cell((s.cg(i,1)-1)*pas1 + min_lo1);

    raw(3+i,21) = num2cell((s.cg(i,2)-1)*pas2 + min_lo2);

    raw(3+i,20) = num2cell(s.cg(i,3));
    %%
    %Centre Grav
    %%
    raw(3+i,8) = num2cell((s.cgm(i,1)-1)*pas2 + min_lo2);

    raw(3+i,9) = num2cell((s.cgm(i,2)-1)*pas1 + min_lo1);
    %%
    %Feret
    raw(3+i,13) = num2cell(s.frt(i,1));


    raw(3+i,14) = num2cell(s.frt(i,2));

    raw(3+i,15) = num2cell(s.frt(i,2)/s.frt(i,1));

    raw(3+i,16) = num2cell(s.frt(i,3));
end
%raw() = s.nbr_couche;
name_file = strcat(rep_echtll,'.xlsx');
xlswrite(name_file ,raw, 'Données')

%s = load(file_name);
%raw() = s.data;
%raw() = s.data_masq;
%disp(data(1,:))
raw2(2,3) = data(1);

if (cellstr(data(1)) == "2 - Echantillion")
    raw2(3,3) = data(17);
else
    raw2(3,3) = data(2);
end

raw2(4,3) = data(3);

raw2(5,3) = data(4);

raw2(6,3) = data(5);
    
raw2(7,3) = data(6);

raw2(8,3) = data(7);
    
raw2(9,3) = data(8);
    
raw2(10,3) = data(9);

raw2(11,3) = data(10);

raw2(12,3) = data(11);

raw2(13,3) = data(12);

raw2(14,3) = data(13);

raw2(14,4) = data(14);
%raw() = s.nbr_couche;
raw2(15,3) = data(15);
xlswrite(name_file ,raw2, 'Information')
end