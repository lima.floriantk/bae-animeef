function ret = analyse_masque(name,data,data_masq,nbr_couche,data1,rep_echtll)

x = size(data,1);
y = size(data,2);

srfc = analyse_surface(data_masq,nbr_couche,x,y);

smm = ITot(data,data_masq,nbr_couche,x,y);

moy = IMoy(data,data_masq,nbr_couche,x,y);

prmtr = analyse_perimetre(data_masq,nbr_couche,x,y);
%disp (prmtr);
idc_crcl = indice_circula(prmtr,srfc,nbr_couche);

cg = Centre_pics(data,data_masq,nbr_couche,x,y);

ecrt_tp = Sig(data,data_masq,nbr_couche,x,y);

pstn = analyse_position(data_masq,nbr_couche,x,y);
%disp (pstn);
nbr_obj = cpt_objet(data_masq,nbr_couche,x,y);
%disp(nbr_obj)
s_obj = sep_objet(data_masq,nbr_couche);
%disp(s_obj(:,:,1))

cgm = ctr_grv(data_masq,nbr_couche);

frt = analyse_feret(data_masq, nbr_couche);

ret = struct('data',data,'data_masq',data_masq,'srfc',srfc,'pstn',pstn,'smm',smm,'prmtr',prmtr,'nbr_obj',nbr_obj,'s_obj',s_obj,'moy',moy,'idc_crcl',idc_crcl,'ecrt_tp',ecrt_tp,'cg',cg,'nbr_couche',nbr_couche,'data1',data1,'cgm',cgm,'frt',frt);

chmn_dnn = strcat('Dnn/',cellstr(data1(15)),"_",cellstr(data1(16)),"_",cellstr(data1(2)),"_",name, '.mat');

rep_echtll = strcat(rep_echtll,'/',name);

save(chmn_dnn,'data','data1','data_masq','srfc','pstn','smm','prmtr','nbr_obj','s_obj','moy','idc_crcl','ecrt_tp','cg','nbr_couche','data1','cgm','frt');
dnnto_excel(chmn_dnn,data1,rep_echtll);
dnn_masque_excel(data,data_masq,nbr_couche,chmn_dnn,rep_echtll,data1)

%xlswrite("Test.xlsx");
end



function dnn_masque_excel(data,data_masq,nbr_couche,file_name,rep_echtll,data1)
x = size(data_masq,1);
y = size(data_masq,2);
nbr_obj = cpt_objet(data_masq,nbr_couche,x,y);
disp(file_name)
name_file = strcat(rep_echtll,'.xlsx');
[~, ~, raw] = xlsread(name_file,'Données');
nbr_fils = 0;
%nbr_obj_max = recup_nbr_obj_max(nbr_obj,nbr_couche);
for i = 1:nbr_couche
    data_label = bwlabel(data_masq(:,:,i));
    %disp('Objet num :')
    %disp (i);
    if nbr_obj(i) > 1
        for j = 1:nbr_obj(i)

            data_obj = recup_obj(data_label,j,x,y);
            if (veref_obj(data_obj) == 1)
                raw(nbr_couche + 5 + nbr_fils,1) = cellstr(strcat('Obj : ',int2str(i),' - ',int2str(j)));

                srfc = analyse_surface(data_obj,1,x,y);

                raw(nbr_couche + 5 + nbr_fils,11) = num2cell(srfc);

                pstn = analyse_position(data_obj,1,x,y);

                min_lo1 = cell2mat(data1(4));
                min_lo2 = cell2mat(data1(6));
                pas1 = cell2mat(data1(8));
                pas2 = cell2mat(data1(9));

                raw(nbr_couche + 5 + nbr_fils,4) = num2cell((pstn(1)-1)*pas2 + min_lo2);

                raw(nbr_couche + 5 + nbr_fils,5) = num2cell((pstn(2)-1)*pas1 + min_lo1);

                raw(nbr_couche + 5 + nbr_fils,6) = num2cell((pstn(3)-1)*pas2 + min_lo2);

                raw(nbr_couche + 5 + nbr_fils,7) = num2cell((pstn(4)-1)*pas1 + min_lo1);

                raw(nbr_couche + 5 + nbr_fils,17) = num2cell(ITot(data,data_obj,1,x,y));

                prmtr = analyse_perimetre(data_obj,1,x,y);

                raw(nbr_couche + 5 + nbr_fils,10) = num2cell(prmtr);

                raw(nbr_couche + 5 + nbr_fils,18) = num2cell(IMoy(data,data_obj,1,x,y));

                raw(nbr_couche + 5 + nbr_fils,12) = num2cell(indice_circula(prmtr,srfc,1));

                raw(nbr_couche + 5 + nbr_fils,19) = num2cell(Sig(data,data_obj,1,x,y));

                cg = Centre_pics(data,data_obj,1,x,y);

                raw(nbr_couche + 5 + nbr_fils,22) = num2cell((cg(1)-1)*pas1 + min_lo1);

                raw(nbr_couche + 5 + nbr_fils,21) = num2cell((cg(2)-1)*pas2 + min_lo2);

                raw(nbr_couche + 5 + nbr_fils,20) = num2cell(cg(3));

                cgm = ctr_grv(data_obj,1);

                raw(nbr_couche + 5 + nbr_fils,8) = num2cell((cgm(1)-1)*pas2 + min_lo2);

                raw(nbr_couche + 5 + nbr_fils,9) = num2cell((cgm(2)-1)*pas1 + min_lo1);

                frt = analyse_feret(data_obj,1);

                raw(nbr_couche + 5 + nbr_fils,13) = num2cell(frt(1,1));

                raw(nbr_couche + 5 + nbr_fils,14) = num2cell(frt(1,2));

                raw(nbr_couche + 5 + nbr_fils,15) = num2cell(frt(1,2)/frt(1,1));

                raw(nbr_couche + 5 + nbr_fils,16) = num2cell(frt(1,3));

                nbr_fils = nbr_fils + 1;
            end
        end
    end
end
name_file = strcat(rep_echtll,".xlsx");
xlswrite(name_file ,raw, 'Données')
end



function ret = analyse_surface(data,nbr_couche,x,y)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if (data(j,k,i) == 1)
                ret(i) = ret(i) + 1;
            end
        end
    end
end

end



function ret = ITot(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if (data_masq(j,k,i) == 1)
                ret(i) = ret(i) + data(j,k);
            end
        end
    end
end

end



function ret = IMoy(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    cpt = 0;
    for j = 1:x
        for k = 1:y
            if (data_masq(j,k,i) == 1)
                ret(i) = ret(i) + data(j,k);
                cpt = cpt + 1;
            end
        end
    end
    ret(i) = ret(i)/cpt ;
end

end



function ret = analyse_perimetre(data_masq,nbr_couche,x,y)
BW = zeros(x,y,nbr_couche);

for i = 1:nbr_couche
    BW(:,:,i) = bwperim(data_masq(:,:,i));
end

ret = analyse_surface(BW,nbr_couche,x,y);
end



function ret = indice_circula(perimetre,air,index)
ret = zeros(index,1);

for i = 1:index
    ret(i) = (perimetre(i) * perimetre(i)) / (4 * pi *air(i));
end

end



function ret = Centre_pics(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,3);
for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if ( data_masq(j,k,i) == 1)
                if ret(i,1) == 0 && ret(i,2) == 0
                    ret(i,1) = j;
                    ret(i,2) = k;
                    ret(i,3) = data(j,k);
                end
                if data(j,k) > data(ret(i,1),ret(i,2))
                    ret(i,1) = j;
                    ret(i,2) = k;
                    ret(i,3) = data(j,k);
                end
            end
        end
    end
end

end



function ret = Sig(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,1);
moyenne = IMoy(data,data_masq,nbr_couche,x,y);

for i = 1:nbr_couche
    cpt = 0;
    for j = 1:x
        for k = 1:y
            if (data_masq(j,k,i) == 1)
                tmp = data(j,k) - moyenne(i);
                if (tmp < 0)
                    tmp = tmp * (-1);
                end
                ret(i) = ret(i) + tmp * tmp ;
                cpt = cpt + 1;
            end
        end
    end
    ret(i) = ret(i)/cpt ;
    ret(i) = sqrt(ret(i));
end

end



%PREND DES MATRICES ET TROUVE LES COORDONNEES H
function ret = analyse_position(data,nbr_couche,x,y)
ret = zeros(nbr_couche,4);

for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if (data(j,k,i) == 1 && (ret(i,1) == 0 || ret(i,1) > k))
                ret(i,1) = k;
            end
            if (data(j,k,i) == 1 && (ret(i,2) == 0 || ret(i,2) > j))
                ret(i,2) = j;
            end
            if (data(j,k,i) == 1 && (ret(i,3) == 0 || ret(i,3) < k))
                ret(i,3) = k;
            end
            if (data(j,k,i) == 1 && (ret(i,4) == 0 || ret(i,4) < j))
                ret(i,4) = j;
            end
        end
    end
end

end



function ret = cpt_objet(data,nbr_couche,x,y)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if data(j,k,i) == 1
                ret(i) = ret(i) + 1;
                data(:,:,i) = supp_objet(data(:,:,i),j,k);
            end
        end
    end
end

end



function ret = sep_objet(data,nbr_couche)
ret = zeros(size(data,1),size(data,2),nbr_couche);

for i = 1:nbr_couche
   ret(:,:,i) = bwlabel(data(:,:,i));
end

contourf(ret(:,:,i))
end



function ret = ctr_grv(data,nbr_couche)
ret = zeros(nbr_couche,2);
for i = 1:nbr_couche
    stats = regionprops(data(:,:,i));
    ret(i,1) = round(stats.Centroid(1));
    ret(i,2) = round(stats.Centroid(2));
end
end


function ret = analyse_feret(data_masq,nbr_masq)
ret = zeros(nbr_masq,3);
for i = 1:nbr_masq
    min = bwferet(data_masq(:,:,i),'MinFeretProperties');

    %disp(min)
    ret(i,1) = min.MinDiameter(1);
    %disp(ret)

    max = bwferet(data_masq(:,:,i),'MaxFeretProperties');

    ret(i,2) = max.MaxDiameter(1);

    ret(i,3) = min.MinAngle(1) - max.MaxAngle(1);

    if (ret(i,3) > 180)
        ret(i,3) = ret(i,3) - 360;
    end
    if (ret(i,3) < (-180))
        ret(i,3) = ret(i,3) + 360;
    end
    if (ret(i,3) < 0)
        ret(i,3) = ret(i,3) * -1;
    end
    %disp(ret(i,3))

end
end



























%Fonctions secondaires

function ret = veref_obj(data)
x = size(data,1);
y = size(data,2);
ret = 0;
for i = 1:x
    for j = 1:y
        if (data(i,j) == 1)
            ret = 1;
        end
    end
end
end

function ret = recup_obj(data,nbr,x,y)
ret = data;
for i = 1:x
    for j = 1:y
        if (data(i,j) == nbr)
            ret(i,j) = 1;
        else
            ret(i,j) = 0;
        end
    end
end
end

function ret = supp_objet(data,i,j)

if data(i,j) == 1
    data(i,j) = 0;
    if i > 1
        data = supp_objet(data,i - 1, j);
    end
    if i < size(data,1)
        data = supp_objet(data,i + 1, j);
    end
    if j > 1 && data(i,j - 1) == 1
        data = supp_objet(data,i, j - 1);
    end
    if j < size(data,2) && data(i,j + 1) == 1
        data = supp_objet(data,i, j + 1);
    end
end

ret = data;
end