function ret = union_dnn_conv(data,data1)

%str = char(strcat('Bibliothèque/Dnn/', liste(1),".mat"));
%data = load(str);
%x = size(data.data_masq,1);
%y = size(data.data_masq,2);


ret = union_uni(data,data1);


end


%FAIT    DATA1 U DATA2 
function ret = union_uni(data1,data2)
x1 = size(data1,1);
x2 = size(data2,1);
y1 = size(data1,2);
y2 = size(data2,2);


if (x1 > x2)
    xm = x1;
else
    xm = x2;
end
if (y1 > y2)
    ym = y1;
else
    ym = y2;
end

ret = zeros(xm,ym);

for i = 1:x1
    for j = 1:y1
        if (data1(i,j) >= 1)
            ret(i,j) = ret(i,j) + data1(i,j);
        end
    end
end

for i = 1:x2
    for j = 1:y2
        if (data2(i,j) >= 1)
            ret(i,j) = ret(i,j) + data2(i,j);
        end
    end
end
end