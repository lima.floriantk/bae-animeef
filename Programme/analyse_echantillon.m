function analyse_echantillon(name_file,nbr_couche,new_name)
format long;
data = readmatrix(name_file);

name_file = strrep(name_file,'.xlsx','');
match = wildcardPattern + "/";
name_file = erase(name_file,match);
rep_echtlln = 'Img_masque/' + new_name + '_result';

if exist(rep_echtlln,"dir") == 0
    mkdir(rep_echtlln)
end

data(1,:) = [];
data(:,1) = [];
data(:,1) = [];

new_data = zeros(60, 591);
i = 1;

while i <= 60
    j = 1;
    while j <= 591
        new_data(i,j) = data(j,i);
        j = j+ 1;
    end
    i = i + 1;
end

data = new_data;
contourf(data)
set(gca,'xlim',[0 591], 'xtick', 0:20:591)
set(gca, 'ylim',[1 60], 'ytick',0:5:60)
colormap('gray')

maxGL = max(data(:));

i = 1;
data_msq = zeros(60,591,10);

while i < nbr_couche + 1
    masque = data >= maxGL/(nbr_couche+5) * i;
    contourf(masque)
    data_msq(:,:,i) = masque;
    img = getframe(gcf);
    imwrite(img.cdata,rep_echtlln +'/'+ name_file+'_masque'+int2str(i)+'.png')
    i = i + 1;
end
%chmn_dnn = strcat(Svgd_dnn,name_file);
%save(chmn_dnn,'data_msq')
end


%function r = Img_masque
%r = 'Img_masque/';
%end

%function r = Svgd_union
%r = 'Svgd_union';
%end

%function r = Svgd_dnn
%r = 'Svgd_dnn/';
%end
%%%%%%%%%%%%%%%%%%%%%







%FONTION PERMETTANT D'AJOUTER UN ECHANTILLION A LA BIBLIOTQUE
%PREND UN NOM DE FICHIER ET UN NOMBRE DE COUCHE DE MASQUE REQUIS
%SAUVEGARDE LES RESULTATS DANS LES DOSSIERS DEFINI DANS UNE MACRO




%FONTION PERMETTANT D'AJOUTER LES ECHANTILLIONS D'UN DOSSIER A LA BIBLIOTQUE
%PREND LE NOM D'UN DOSSIER ET LE NBR DE COUCHE DE MASQUE
%EXECUTE LA FONCTION AJOUT ECHANTILLION




%ANALYSE MELANGE ECHANTILLION




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%ANALYSE LES MASQUES
%PREND UNE BASE DE DONNEE ET UN TABLEAU DE MASQUE
function ret = analyse_masque(chmn_dnn,data,data_masq,nbr_couche)

srfc = analyse_surface(data_masq,nbr_couche);
%disp(srfc(2))
smm = ITot(data,data_masq,nbr_couche);

moy = IMoy(data,data_masq,nbr_couche);

%cgm = Centre_grav_masque(data_masq, nbr_couche);

prmtr = analyse_perimetre(data_masq,nbr_couche);
%disp (prmtr);
idc_crcl = indice_circula(prmtr,srfc,nbr_couche);

cg = Centre_grav(data,data_masq,nbr_couche);

ecrt_tp = Sig(data,data_masq,nbr_couche);

pstn = analyse_position(data_masq,nbr_couche);
%disp (pstn);
nbr_obj = cpt_objet(data_masq,nbr_couche);
%disp(nbr_obj)
s_obj = sep_objet(data_masq,nbr_couche);
%disp(s_obj(:,:,1))

save(chmn_dnn,'data','data_masq','srfc','pstn','smm','prmtr','nbr_obj','s_obj','moy','idc_crcl','ecrt_tp','cg','nbr_couche');
ret = struct('data',data,'data_masq',data_masq,'srfc',srfc,'pstn',pstn,'smm',smm,'prmtr',prmtr,'nbr_obj',nbr_obj,'s_obj',s_obj,'moy',moy,'idc_crcl',idc_crcl);

%xlswrite("Test.xlsx");
end


%PREND UNE LISTE DE COMPOSANT ISOLE ET 
%FAIT LA FUSION DES MASQUES DES COMPOSANTS
%EN LES SAUVEGARDANTS
function ret = union_tableau(liste,name_file,nbr_couche)
ret = zeros(60,591,nbr_couche);
str = strcat('Svgd_union/' ,'/', name_file);

if (exist(str, "dir") == 0)
    mkdir (str)
end

for i = 1:nbr_couche
    for j = 1:numel(liste)
        str = strcat('svgd_dnn/', liste(j));
        disp (str)
        data = load(str);
        ret = union_masque(ret,data.data_masq,nbr_couche);
        contourf(ret(:,:,i))
        colormap('gray')
        img = getframe(gcf);
        imwrite(img.cdata, strcat('Svgd_union/' ,'/', name_file , '/' , name_file , '_masque' , int2str(i) , '.png'))
    end
end

end


%FAIT    DATA1 U DATA2 
function ret = union_masque(data1,data2,nbr_couche)
ret = zeros(60,591,nbr_couche);

for i = 1:nbr_couche
    for j = 1:60
        for k = 1:591
            if (data1(j,k,i) == 1 || data2(j,k,i) == 1)
                ret(j,k,i) = 1;
            end
        end
    end
end

end


function ret = analyse_surface(data,nbr_couche)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    for j = 1:60
        for k = 1:591
            if (data(j,k,i) == 1)
                ret(i) = ret(i) + 1;
            end
        end
    end
end

end



%function ret = recup_objet(data,obj,index)
%ret = zeros(60,591);
%
%for i = 1:60
%    for j = 1:591
%        if (data(i,j,index) == obj)
%            ret(60,591) = 1;
%        end
%    end
%end
%end



%PREND DES MATRICES ET TROUVE LES COORDONNEES H
function ret = analyse_position(data,nbr_couche)
ret = zeros(nbr_couche,4);

for i = 1:nbr_couche
    for j = 1:60
        for k = 1:591
            if (data(j,k,i) == 1 && (ret(i,1) == 0 || ret(i,1) > k))
                ret(i,1) = k;
            end
            if (data(j,k,i) == 1 && (ret(i,2) == 0 || ret(i,2) > j))
                ret(i,2) = j;
            end
            if (data(j,k,i) == 1 && (ret(i,3) == 0 || ret(i,3) < k))
                ret(i,3) = k;
            end
            if (data(j,k,i) == 1 && (ret(i,4) == 0 || ret(i,4) < j))
                ret(i,4) = j;
            end
        end
    end
end

end



function ret = analyse_perimetre(data_masq,nbr_couche)
BW = zeros(60,591,nbr_couche);

for i = 1:nbr_couche
    BW(:,:,i) = bwperim(data_masq(:,:,i));
end

ret = analyse_surface(BW,nbr_couche);
end

function ret = supp_objet(data,i,j)

if data(i,j) == 1
    data(i,j) = 0;
    if i > 1
        data = supp_objet(data,i - 1, j);
    end
    if i < 60
        data = supp_objet(data,i + 1, j);
    end
    if j > 1 && data(i,j - 1) == 1
        data = supp_objet(data,i, j - 1);
    end
    if j < 591 && data(i,j + 1) == 1
        data = supp_objet(data,i, j + 1);
    end
end

ret = data;
end


function ret = cpt_objet(data,nbr_couche)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    for j = 1:60
        for k = 1:591
            if data(j,k,i) == 1
                ret(i) = ret(i) + 1;
                data(:,:,i) = supp_objet(data(:,:,i),j,k);
            end
        end
    end
end

end



function ret = sep_objet(data,nbr_couche)
ret = zeros(60,591,nbr_couche);

for i = 1:nbr_couche
   ret(:,:,i) = bwlabel(data(:,:,i));
end

contourf(ret(:,:,i))
end



%function ret = analyse_feret(data_masq)
%end



function ret = ITot(data,data_masq,nbr_couche)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    for j = 1:60
        for k = 1:591
            if (data_masq(j,k,i) == 1)
                ret(i) = ret(i) + data(j,k);
            end
        end
    end
end

end

function ret = IMoy(data,data_masq,nbr_couche)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    cpt = 0;
    for j = 1:60
        for k = 1:591
            if (data_masq(j,k,i) == 1)
                ret(i) = ret(i) + data(j,k);
                cpt = cpt + 1;
            end
        end
    end
    ret(i) = ret(i)/cpt ;
end

end



function ret = Analyse_pics(data,data_masq,nbr_couche)
ret = zeros(nbr_couche,1);
end



function ret = Recup_data_masq(data,data_masq,nbr_couche)
ret = zeros(60,591,nbr_couche);

for i = 1:nbr_couche
    for j = 1:60
        for k = 1:591
            if (data_masq(j,k,i) == 1)
                ret(j,k,i) = data(j,k,i);
            end
        end
    end
end

end



%function ret = Centre_grav_masque(data, nbr_couche)
%ret = zeros(nbr_couche,2);
%for i = 1:nbr_couche
%    stats = regionprops('table',data(i),'centroid');
%    ret(i,1) = stats.Centroid(1,1); 
%    ret(i,2) = stats.Centroid(1,2); 
%end
%end

function ret = Sig(data,data_masq,nbr_couche)
ret = zeros(nbr_couche,1);
moyenne = IMoy(data,data_masq,nbr_couche);

for i = 1:nbr_couche
    cpt = 0;
    for j = 1:60
        for k = 1:591
            if (data_masq(j,k,i) == 1)
                tmp = data(j,k) - moyenne(i);
                if (tmp < 0)
                    tmp = tmp * (-1);
                end
                ret(i) = ret(i) + tmp * tmp ;
                cpt = cpt + 1;
            end
        end
    end
    ret(i) = ret(i)/cpt ;
    ret(i) = sqrt(ret(i));
end

end


function ret = Centre_grav(data,data_masq,nbr_couche)
ret = zeros(nbr_couche,2);

for i = 1:nbr_couche
    for j = 1:60
        for k = 1:591
            if ( data_masq(j,k,i) == 1)
                if ret(i,1) == 0 || ret(i,2) == 0
                    ret(i,1) = j;
                    ret(i,2) = k;
                end
                if data(j,k) > data(ret(i,1),ret(i,2))
                    ret(i,1) = j;
                    ret(i,2) = k;
                end
            end
        end
    end
end

end

function ret = indice_circula(perimetre,air,index)
ret = zeros(index,1);

for i = 1:index
    ret(i) = (perimetre(i) * perimetre(i)) / (4 * pi *air(i));
end

end