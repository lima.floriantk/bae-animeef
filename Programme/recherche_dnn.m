function ret1 = recherche_dnn(data,name_dir)
ret1 = [];
chemin = fullfile(strdir,name_dir);
liste = dir(chemin);
liste = liste(3:end);
for i = 1:numel(liste)
    if (isfolder(liste(i).name))
        ret2 = recherche_dnn(data,liste(i).name);
        ret1 = [ret1 , ret2];
    end
    cndtn_recherche_dnn(data,name_dir)
    %Insertion des conditions
    %if ()
    %end
end
end

function cndtn_recherche_dnn(data,name_dir)
load(name_dir)

end