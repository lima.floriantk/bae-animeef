function ajout_echantillon(name_file,nbr_couche,new_tab)
format longg;

new_name = char(new_tab(1));

data = readmatrix(name_file);

if (contains(name_file, ".txt"))
    data(isnan(data)) = 0;
end

new_name = strrep(new_name,'.xlsx','');
new_name = strrep(new_name,'\','/');
[~,new_name] = fileparts(new_name);

str = "Dnn";
if (exist(str,"dir") == 0)
    mkdir(str)
end

str = strcat("Bibliothèque/",new_tab(15),'/',new_tab(16));
if (exist(str,"dir") == 0)
    mkdir(str)
end
disp(str)

str = strcat(str,'/',new_tab(2));
if (exist(str,"dir") == 0)
    mkdir(str)
end
chemin = strcat(str,'/');
rep_echtlln = strcat(chemin, new_name , '_result');


if exist(rep_echtlln,"dir") == 0
    mkdir(rep_echtlln)
end

%NOTE SUPPRIMER LES LIGNES ET COLONNES INUTILES

while (isstring(data(1,1)))
    data(1,:) = [];
end
data(1,:) = [];
data(:,1) = [];

new_data = zeros(size(data,2), size(data,1),"double");
i = 1;


while i <= size(data,2)
    j = 1;
    while j <= (size(data,1))
        if (data(j,i) >= 0)
            new_data(i,j) = data(j,i);
        else
            new_data(i,j) = 0;
        end
        j = j+ 1;
    end
    i = i + 1;
end
data = new_data;

y=zeros(size(data,1),1);
x=zeros(size(data,2),1);
for n = 1:size(data,1)
    y(n) = cell2mat(new_tab(4)) + n * cell2mat(new_tab(8));
end
for n = 1:size(data,2)
    x(n) = cell2mat(new_tab(6)) + n * cell2mat(new_tab(9));
end

colormap('gray')

contourf(x,y,data)
img = getframe(gcf);
imwrite(img.cdata,strcat(rep_echtlln,'/', new_name,'_original.png'))

maxGL = max(data(:));

i = 1;
data_msq = zeros(size(data,1),size(data,2),nbr_couche);

while i <= nbr_couche
    masque = data >= maxGL/(nbr_couche+5) * i;
    contourf(x,y,masque)
    data_msq(:,:,i) = masque;
    img = getframe(gcf);
    imwrite(img.cdata,strcat(rep_echtlln,'/', new_name,'_masque',int2str(i),'.png'))
    i = i + 1;
end
analyse_masque(new_name,data, data_msq,nbr_couche,new_tab,rep_echtlln);
end
