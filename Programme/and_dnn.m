function [ret,srfc] = and_dnn(data1,data2)
x1 = size(data1,1);
x2 = size(data2,1);
y1 = size(data1,2);
y2 = size(data2,2);

if (x1 > x2)
    xm = x1;
    xx = x2;
else
    xm = x2;
    xx = x1;
end
if (y1 > y2)
    ym = y1;
    yy = y2;
else
    ym = y2;
    yy = y1;
end
%disp(length(find(data1==1)))
%disp(length(find(data2==1)))
srfc = 0;
ret = zeros(xm,ym);

for i = 1:xx
    for j = 1:yy
        if (data2(i,j) == 1)
            if (data1(i,j) == 1)
                ret(i,j) = 1;
            end
            srfc = srfc +1;
        end
    end
end
end